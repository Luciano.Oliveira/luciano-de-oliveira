<?php
// Conexão com o banco de dados
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "database_name";

$conect = new mysqli($servername, $username, $password, $dbname);


$sql = "SELECT Tb_banco.nome, Tb_convenio.verba, Tb_contrato.codigo, Tb_contrato.data_inclusao, Tb_contrato.valor, Tb_contrato.prazo
        FROM Tb_banco
        INNER JOIN Tb_convenio ON Tb_banco.codigo = Tb_convenio.banco
        INNER JOIN Tb_convenio_servico ON Tb_convenio.codigo = Tb_convenio_servico.convenio
        INNER JOIN Tb_contrato ON Tb_convenio_servico.codigo = Tb_contrato.convenio_servico";

$result = $conect->query($sql);

if ($result->num_rows > 0) {

  echo "<table><tr><th>Nome do Banco</th><th>Verba</th><th>Código do Contrato</th><th>Data de Inclusão</th><th>Valor</th><th>Prazo</th></tr>";
  

  while($row = $result->fetch_assoc()) {

    echo "<tr><td>" . $row["nome"] . "</td><td>" . $row["verba"] . "</td><td>" . $row["codigo"] . "</td><td>" . $row["data_inclusao"] . "</td><td>" . $row["valor"] . "</td><td>" . $row["prazo"] . "</td></tr>";
  }
  
  echo "</table>";
} else {
  echo "Nenhum resultado encontrado.";

}
$conect->close();
?>
